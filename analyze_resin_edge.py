
# ** Imports ** #
import os
import logging
import errno
from typing import Generator, NoReturn, Tuple
import numpy as np
import pandas as pd
import re
import csv
import datetime
import itertools
from scipy import signal, stats
from matplotlib import pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from contextlib import contextmanager
from collections import deque
# **____________________** #


# ** Parameter ** #
ALLOWED_PERCENTAGE_BAD_LINES = 33
"""
ALLOWED_PERCENTAGE_BAD_LINES:
    Possible Values: R[0, 100]

    Explanation: The number of lines in a given measurement which can't be used for analysis as a percentage of the total number of lines in the measurement. For example: 33 means that, by a total count of 100 lines at maximum 33 lines in the measurement can be skipped in the analysis. When exceeding the given percentage a warning is thrown with the additional information on what measurement of what sample how many lines were skipped.

    DEFAULT:    33
"""
DEFAULT_ETCHING_TIME_MIN = 5
"""
DEFAULT_ETCHING_TIME_MIN:
    Possible Values: R(0, inf)

    Explanation: The time the samples were etched in minutes. Notice: if yous samples had different etching times then in this default, you can specify there times in ETCHING_TIME_MIN

    DEFAULT:    5
"""
ETCHING_TIME_MIN = {"A1": 10}
"""
ETCHING_TIME_MIN:
    Possible Values: Dictionary {"Sample name 1": value, "Sample name 2": value, ...}

    Explanation: The etching time of your samples in minutes, when given for each sample individually. If all your samples had the same etching time, you can give this constant the default value and give the time in minutes in DEFAULT_ETCHING_TIME_MIN. Notice: you must give at least the default value for the program to work. The samplename must be the same as in your measurementfile. For example: A1_1_... : means sample A1 measurement 1. When sample A1 was etched for 10 minutes the dictionary entry would be: "A1": 10

    DEFAULT:    {}
"""
HEADER_LINES = 4
"""
HEADER_LINES:
    Possible Values: N[0, inf)

    Explanation: Number of lines in your files, before the actual data starts.

    DEFAULT:    4
"""
LOG_FORMAT_FILE = '%(levelname)s - %(asctime)s: %(message)s'
LOG_FORMAT_STREAM = '%(levelname)s - %(asctime)s: %(message)s'
"""
LOG_FORMAT_XXXX:
    Possible Values: Look for possible string formats in the documentation of the python logging module

    Explanation: The way entry's in the logging file and the log stream are formatet. For example '%(message)s' tells the logger to display the log massage, which is the least you want to see. '%(levelname)s' tells the logger to show wether the massage was thrown from an info, a warning, an error, etc.

    DEFAULT:    '%(levelname)s - %(asctime)s: %(message)s'
"""
DEBUG_LEVEL_FILE = 10
DEBUG_LEVEL_STREAM = 20
"""
DEBUG_LEVEL_XXXX:
    Possible Values:    0,      10,     20,     30,     40,     50

    Explanation: Value sets the logging mode. All log messages whose type has at least the selected value are displayed.
    DEBUG_LEVEL_STREAM configures the log level displayed in the console.
    DEBUG_LEVEL_FILE configures the log level written in the log file.

        NOT SET = 0    Debugging = 10     Info = 20
        Warning = 30   Error = 40         Critical = 50

    DEFAULT:    20
"""
SHOW_LINE_PLOTS = False
"""
SHOW_LINE_PLOTS:
    Possible Values:    True,   False

    Explanation: If set to True a plot that shows the propability density function is displayed for every line of each sample. WARNING: Only useful for debugging. It is possible to invoke hundreds of plots for each sample with this option.

    DEFAULT:    False
"""
SHOW_DISTANCE_PROPABILITY = False
"""
SHOW_DISTANCE_PROPABILITY:
    Possible Values:    True,   False

    Explanation: If set to True a plot that shows the propability density function for the edge hight is displayed for every sample.

    DEFAULT:    False
"""
ENABLE_CSV_OUTPUT = True
"""
ENABLE_CSV_OUTPUT:
    Possible Values:    True,   False

    Explanation: If set to True a csv file is created with at least the modus based analysis of the given samples. Additional output can be configured with the next next parameter (CALCULATION_ADDITIONAL_OUTPUTS).

    DEFAULT:    True
"""
CALCULATION_ADDITIONAL_OUTPUTS = "00"
"""
CALCULATION_OUTPUTS:
    Possible values:     binary 00 - 11

    Explanation: Additional calculationmode activation binary coded. From most significant to least significant bit, the first bit activates mean based output, the second bit activates median based output. High level of bits activate the additional output. For example: 10 activates mean calculation, 11 activates mean and median calculation.

    DEFAULT:       00
"""

CSV_OUTPUT_NAME = r'results_neu'
"""
CSV_OUTPUT_NAME:
    Possible values:     string  # Note some characters aren't supported for filenames on os.

    Explanation: setup filename where the analysis of all your samples are saved
"""
# **____________________** #


# ** Global Variables ** #
logger = -1
datapaths = dict()
sample_keys = list()
# **____________________** #


# ** Functions ** #
@contextmanager
def changeDir(destination) -> NoReturn :

    cwd = os.getcwd()

    try:
        os.chdir(destination)
        yield
    except FileNotFoundError:
        os.makedirs(destination)
        os.chdir(destination)
        yield
    finally:
        os.chdir(cwd)

@contextmanager
def openFile(path: str, mode="r") -> Generator:
    """Opens a file and yields its content.

    Args:
        path (str): path to the file. Absolute path and relative path from current working path is possible.
        mode (str, optional): The mode in which the file is opened. "r" is read only mode. Defaults to "r".

    Yields:
        (generator): Generator object which allows to read the Object line by line.
    """
    f = None
    try:
        f = open(path, mode)
        yield f
    except OSError as e:
        if e.errno == errno.ENOENT:
            logger.error('File not found')
        elif e.errno == errno.EACCES:
            logger.error('Permission denied')
        else:
            logger.error(f'Unexpected error:{e.errno}')
    finally:
        f.close()

def createConfigLogger() -> bool:
    """[summary]
    """
    global DEBUG_LEVEL_FILE
    global DEBUG_LEVEL_STREAM
    global LOG_FORMAT_FILE
    global LOG_FORMAT_STREAM
    global logger

    warning = False
    LOG_LEVEL_FILE = None
    LOG_LEVEL_STREAM = None
    ziplist = zip(
        [0, 10, 20, 30, 40, 50],
        [logging.NOTSET, logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]
        )
    # Translate Levels
    if DEBUG_LEVEL_FILE in [0, 10, 20, 30, 40, 50]:
        for i,j in ziplist:
            if i == DEBUG_LEVEL_FILE:
                LOG_LEVEL_FILE = j
                break
            else:
                continue
    else:
        LOG_LEVEL_STREAM = logging.WARNING
        warning = True

    if DEBUG_LEVEL_STREAM in [0, 10, 20, 30, 40, 50]:
        for i,j in ziplist:
            if i == DEBUG_LEVEL_STREAM:
                LOG_LEVEL_STREAM = j
                break
            else:
                continue
    else:
        LOG_LEVEL_STREAM = logging.WARNING
        warning = True

    # Set up Logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    formatter_file = logging.Formatter(LOG_FORMAT_FILE)
    formatter_stream = logging.Formatter(LOG_FORMAT_STREAM)

    destdir = os.getcwd()
    destdir = os.path.join(destdir, r"AFM Custom Heightdest recognition\log")

    # create log file
    with changeDir(destdir):
        file_handler = logging.FileHandler(r"Custom_Edgehight_recognition.log")

    # Config Log for File
    file_handler.setFormatter(formatter_file)
    file_handler.setLevel(LOG_LEVEL_FILE)

    # Config Log for Stream
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter_stream)
    stream_handler.setLevel(LOG_LEVEL_STREAM)

    # Activate Logs
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    if warning:
        logger.warning('Set value for debug level is not supported. Value is set to 30 as default')
    return True

def readInputDirs() -> bool:
    """Function reads all subdirs in given working dir and puts there path into a dictionary. The Key of the dictionary is the dirname. The value to the key is a list of relative paths from the working dir to the datasets of the subdir.
    """
    global datapaths
    global sample_keys

    with changeDir(r"AFM Custom Heightdest recognition\daten"):

        _dirs = os.listdir()
        logger.debug(f"Samplefolders: {_dirs}")
        for _dir in _dirs:
            if os.path.isdir(_dir):
                sample_keys.append(_dir)
                with changeDir(_dir):
                    datapaths[_dir] = os.listdir()

    return True

def getMeanStats(key_gen: Generator, target_key: str, target_dict: dict, current_key: str) -> dict:
    """[summary]

    Args:
        key_gen ([type]): [description]
        target_key ([type]): [description]
        target_dict ([type]): [description]

    Returns:
        [type]: [description]
    """

    ctr = 0
    modus, mean, median = (0,0,0)
    _tmp_dict = dict()

    for _key in key_gen:
        ctr +=1
        modus += target_dict[_key]["modus"]
        mean += target_dict[_key]["mean"]
        median += target_dict[_key]["median"]

    try:
        modus = modus / ctr
        median = median / ctr
        mean = mean / ctr
    except ZeroDivisionError:
        logger.exception("Devision by Zero at:")
    else:
        _tmp_dict = {target_key + "_modus": modus, target_key + "_median": median, target_key + "_mean": mean}
        logger.info(f"Height for {target_key} is:\nModus:\t{round((_tmp_dict[current_key + '_modus'])*1e9, 1)} nm\nMean:\t\t{round((_tmp_dict[current_key + '_mean'])*1e9, 1)} nm\nMedian:\t{round((_tmp_dict[current_key + '_median'])*1e9, 1)} nm\n")

        return _tmp_dict

def calcStats(_dict: dict) -> dict:
    """[summary]

    Args:
        _dict ([type]): [description]

    Returns:
        [type]: [description]
    """
    global DEFAULT_ETCHING_TIME_MIN
    global ETCHING_TIME_MIN
    _tmp_dict = dict()

    for key in _dict.keys():
        _tmp_dict[key + "_stats"], _tmp_dict[key + "_dist"] = calculateStatistics(_dict[key], key)

        for k, value in ETCHING_TIME_MIN.items():
            if k in key:
                _tmp_dict[key + "_stats"]["etching_time"] = value
                break
        else:
            _tmp_dict[key + "_stats"]["etching_time"] = DEFAULT_ETCHING_TIME_MIN

        logger.debug(f"A total of {int(_tmp_dict[key + '_stats']['count'])} were taken to account in the calculation of the sample {key}.\nThe mean distance is:\t\t\t\t{round((_tmp_dict[key + '_stats']['mean'])*1e9, 1)} nm\nThe standard deviation is:\t{round((_tmp_dict[key + '_stats']['std'])*1e9, 1)} nm\nThe median distance is:\t\t\t{round((_tmp_dict[key + '_stats']['median'])*1e9, 1)} nm\nThe modal distance is:\t\t\t{round((_tmp_dict[key + '_stats']['modus'])*1e9, 1)} nm")

    return _tmp_dict

def calculateEtchRatios(bfe: dict, afe: dict, afl: dict, time: float, key: str) -> dict:
    """[summary]

    Args:
        bfe (dict): [description]
        afe (dict): [description]
        afl (dict): [description]
        time (float): [description]
        key (str): [description]

    Returns:
        dict: [description]
    """
    _etch_ratios = dict()
    modes = list(["mean", "median", "modus"])

    for mode in modes:  # iter calculationmodes
        if (key + "_" + mode) in afl.keys() and (key + "_" + mode) in afe.keys() and (key + "_" + mode) in bfe.keys():
            h_be = bfe[key + "_" + mode]
            h_ae = afe[key + "_" + mode]
            h_al = afl[key + "_" + mode]

            if (h_ae - h_al) < 0:   # Check possibility of etching rate calculation
                logger.error(f"Etching rate calculation based on {mode} failed for {key}. Calculated hight for after lift off ({round((h_al*1e9), 1)} nm) is greater then calculated hight for after etching ({round((h_ae*1e9), 1)} nm)")
                _etch_ratios[mode + "_diamant"] = h_al / time if h_al / time > 0 else np.NaN
                _etch_ratios[mode + "_resin"] = np.NaN
                _etch_ratios[mode + "_alpha"] = np.NaN
                _etch_ratios[mode + "_beta"] = np.NaN
                continue
            elif ((h_ae - h_al) - h_be) > 0:
                logger.error(f"Etching rate calculation based on {mode} failed for {key}. Calculated hight for resin after etching ({round(((h_ae - h_al) - h_be)*1e9, 1)} nm) is greater then calculated hight for before etching ({round(h_be*1e9, 1)} nm).")
                _etch_ratios[mode + "_diamant"] = h_al / time if h_al / time > 0 else np.NaN
                _etch_ratios[mode + "_resin"] = np.NaN
                _etch_ratios[mode + "_alpha"] = np.NaN
                _etch_ratios[mode + "_beta"] = np.NaN
                continue
            else:   # Calculate etching rates for resin and diamant
                if h_ae - h_be < 0:
                    logger.warning(f"Calculated hight before etching ({round((h_be*1e9), 1)} nm) is greater then calculated hight after etching ({round((h_ae*1e9), 1)} nm)")

                _etch_ratios[mode + "_diamant"] = h_al / time if h_al / time > 0 else np.NaN
                _etch_ratios[mode + "_resin"] = abs((h_ae - h_al) - h_be) / time

                try:    # Calculate beta etching rate coefficient5
                    _etch_ratios[mode + "_beta"] = _etch_ratios[mode + "_resin"] / _etch_ratios[mode + "_diamant"]
                except ZeroDivisionError:
                    logger.error(f"Calculated etching rate for diamant was zero. Calculation of of etching rate coefficient beta is not possible.")
                    _etch_ratios[mode + "_beta"] = np.NaN

            try:    # Calculating alpha etching rate coefficient
                _etch_ratios[mode + "_alpha"] = h_be / h_ae
            except ZeroDivisionError:
                    logger.error(f"Calculated hight for after etching was zero. Calculation of of etching rate coefficients beta and alpha  is not possible.")
                    _etch_ratios[mode + "_alpha"] = np.NaN
                    # _etch_ratios[mode + "_beta"] = np.NaN
            else:
                logger.debug(f"Etching rate coefficient beta is {round(_etch_ratios[mode + '_beta'], 2)} based on {mode}")
                logger.debug(f"Etching rate coefficient alpha is {round(_etch_ratios[mode + '_alpha'], 2)} based on {mode}")

        else:   # handle calculations with missing data
            _etch_ratios[mode + "_resin"] = np.NaN
            _etch_ratios[mode + "_diamant"] = (afl[key + "_" + mode] / time) if (key + "_" + mode) in afl.keys() else np.NaN

            if (key + "_" + mode) in bfe.keys() and (key + "_" + mode) in afe.keys():
                _etch_ratios[mode + "_alpha"] = bfe[key + "_" + mode] / afe[key + "_" + mode]
            else:
                _etch_ratios[mode + "_alpha"] = np.NaN

            _etch_ratios[mode + "_beta"] = np.NaN

    return _etch_ratios

def calculateStatistics(data: pd.DataFrame, key: str) -> Tuple[dict, pd.Series]:
    """[summary]

    Args:
        data ([type]): [description]
        key ([type]): [description]

    Returns:
        [type]: [description]
    """
    global ALLOWED_PERCENTAGE_BAD_LINES
    global SHOW_LINE_PLOTS
    global SHOW_DISTANCE_PROPABILITY

    kernel_gen = kde_generator(data)
    distance = list()
    peaks = list()

    for kernel in kernel_gen:
        try:
            kernel = next(kernel_gen)
            space = np.linspace(min(kernel.dataset[0]), max(kernel.dataset[0]), num=1000)
            values = kernel.evaluate(space)
            peak = peakFinder(values)
            peaks.append(peak)

            if len(peak) == 2:
                distance.append((abs(space[peak[0]] - space[peak[1]])))

                if SHOW_LINE_PLOTS:
                    fig, ax = plt.subplots()
                    plt.grid(which="both", axis="both", color="gray", alpha=0.8, linestyle="-", linewidth=0.5)
                    ax.plot(space*1e9, values/1e9, label="PDF")
                    for p in peak:
                        ax.vlines(space[p]*1e9, 0, max(values/1e9), linewidth=0.5, color="red", label="Erkanntes Maximum")
                    if len(peak) == 2:
                        ax.hlines(max(values/1e9)*0.1, space[peak[0]]*1e9, space[peak[1]]*1e9, color="green", label="Kantenhöhe", linestyles="dashed")
                        ax.text(space[peak[1]]*1e9 - space[peak[0]]*1e9, max(values/1e9)*0.11, f"Kantenhöhe: {str(round(space[ peak[1]]*1e9 - space[peak[0]]*1e9, 1))} nm")
                    ax.set_ylim(0)
                    ax.set_xlim(0)
                    ax.set_title("Warscheinlichkeitsdichteverteilung")
                    ax.set_xlabel("Höhe [nm]")
                    ax.set_ylabel("Warscheinlichkeitsdichte [1/nm]")
                    ax.xaxis.set_minor_locator(AutoMinorLocator())
                    ax.yaxis.set_minor_locator(AutoMinorLocator())
                    ax.legend(loc="upperleft", fontsize="x-small")
                    plt.tight_layout()
                    plt.show()
                    fig.clear()
                    plt.close(fig)
            else:
                if SHOW_LINE_PLOTS:
                    fig, ax = plt.subplots()
                    plt.grid(which="both", axis="both", color="gray", alpha=0.8, linestyle="-", linewidth=0.5)
                    ax.plot(space*1e9, values/1e9, label="PDF")
                    for p in peak:
                        ax.vlines(space[p]*1e9, 0, max(values/1e9), linewidth=0.5, color="red", label="Erkanntes Maximum")

                    ax.set_ylim(0)
                    ax.set_xlim(0)
                    ax.set_title("Warscheinlichkeitsdichteverteilung")
                    ax.set_xlabel("Höhe [nm]")
                    ax.set_ylabel("Warscheinlichkeitsdichte [1/nm]")
                    ax.xaxis.set_minor_locator(AutoMinorLocator())
                    ax.yaxis.set_minor_locator(AutoMinorLocator())
                    ax.legend(loc="upperleft", fontsize="x-small")
                    plt.tight_layout()
                    plt.show()
                    fig.clear()
                    plt.close(fig)

        except StopIteration:
            break

    logger.debug(f"{key} analyse is completed")
    failed = [x for x in peaks if len(x) != 2]
    if len(failed) > (len(peaks) * (ALLOWED_PERCENTAGE_BAD_LINES / 100)):
        logger.warning(f"In measurement {key} more then {ALLOWED_PERCENTAGE_BAD_LINES}% of the analysed lines could not meet the conditions. From {len(peaks)} analyzed lines, {len(failed)} lines failed.\n")
    else:
        logger.debug(f"In Measurement {key} from {len(peaks)} analyzed lines, {len(failed)} could not meet the conditions.")

    distance = pd.Series(distance, name=key)
    stat_data = dict(distance.describe())
    stat_data["median"] = distance.median()

    kernel = stats.gaussian_kde(distance)
    _space = np.linspace(min(kernel.dataset[0]), max(kernel.dataset[0]), num=1000)
    _values = kernel.evaluate(_space)
    stat_data["modus"] = _space[np.argmax(_values)]

    if SHOW_DISTANCE_PROPABILITY:
        fig, ax = plt.subplots()
        plt.grid(which="both", axis="both", color="gray", alpha=0.8, linestyle="-", linewidth=0.5)
        ax.plot(_space*1e9, _values/1e9, label="PDF")
        ax.vlines(_space[np.argmax(_values)]*1e9, 0, max(_values)/1e9, color="red", label="Modalwert", linewidth=0.5)
        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        ax.set_xlim(0)
        ax.set_ylim(0)
        ax.set_title("Warscheinlichkeitsdichteverteilung über alle Linien einer Probe")
        ax.set_ylabel("Warscheinlichkeitsdichte [1/nm]")
        ax.set_xlabel("Kantenhöhe [nm]")
        ax.legend(loc="upperleft", fontsize="x-small")
        plt.show()

    return stat_data, distance

def kde_generator(data) -> Generator:
    """[summary]

    Args:
        data ([type]): [description]

    Yields:
        [type]: [description]
    """
    for col in data.columns:
        yield stats.gaussian_kde(data[col])

def peakFinder(data: np.array) -> np.array:
    """The Function identifies peaks in given dataset with the "find_peaks" function of the scipy.signal module. It returns position of the peaks as an array. For more information look at the documentation of scipy.

    Args:
        data (np.Array): [description]

    Returns:
        np.Array: [description]
    """
    peaks, _ = signal.find_peaks(data)
    return peaks

def WriteDictToCSV(dict_data: dict, mean=False, median=False, **kwargs) -> str:

    global CSV_OUTPUT_NAME

    date = datetime.datetime.today().strftime ('%Y_%m_%d')
    currentPath = os.getcwd()
    path = os.path.join(currentPath, r"AFM Custom Heightdest recognition\results")
    path = os.path.join(path, date)

    with changeDir(path):
        try:
            with open(CSV_OUTPUT_NAME + ".csv", 'x') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerows([
                ["Analysed Samples:", list(dict_data.keys()), "", "",""],
                ["Unit etching rate", "nm/min", "", "",""],
                ["", "", "", "",""]
                ])

                keys = list(dict_data.keys())
                csv_columns = list(dict_data[keys[0]].keys())
                csv_columns.append("Samples")
                csv_columns = deque(csv_columns)
                csv_columns.rotate(1)
                dictwriter = csv.DictWriter(csvfile, fieldnames=csv_columns)
                dictwriter.writeheader()
                writer.writerow(
                ["Results for modus based calculation of Etching rates", "", "", "",""]
                )
                for sample, value_dict in dict_data.items():
                    value_dict["Samples"] = sample
                    dictwriter.writerow(value_dict)

                if mean:
                    writer.writerow(
                    ["Results for mean based calculation of Etching rates", "", "", "",""]
                    )
                    for sample, value_dict in kwargs["meanbase"].items():
                        value_dict["Samples"] = sample
                        dictwriter.writerow(value_dict)

                if median:
                    writer.writerow(
                    ["Results for median based calculation of Etching rates", "", "", "",""]
                    )
                    for sample, value_dict in kwargs["medianbase"].items():
                        value_dict["Samples"] = sample
                        dictwriter.writerow(value_dict)
            return "0"
        except IOError as e:
            return e

def main() -> NoReturn:
    global sample_keys

    before_etch = dict()
    after_etch = dict()
    after_lift = dict()
    etch_ratio_dict = dict()
    median_etch_ratio_dict = dict()
    mean_etch_ratio_dict = dict()
    error = 0
    error_counter = 0

    if createConfigLogger():
        logger.info("logger is set up")
    else:
        print("WARNING: Logger setup failed")
        exit()

    if readInputDirs():
        logger.info(f"Found {len(datapaths.keys())} sample dirs.")
    else:
        logger.critical("Could not read Sampledirs")
        exit()

    for key in sample_keys:
        _be_ctr = 0
        _ae_ctr = 0
        _al_ctr = 0
        with changeDir(f"AFM Custom Heightdest recognition\\daten\\{key}"):
            logger.info(f"There are {len(datapaths[key])} files to analyse in sample dir {key}.")
            cstf = lambda number: list(map(float, number.split("\t")))
            clm = lambda length: length * round(width/len(data[0])*1e3)
            for _file in datapaths[key]:
                with openFile(_file) as f:
                    header = list(itertools.islice(f, HEADER_LINES))
                    data = [cstf(line) for line in f.readlines()]

                width = float(re.findall(r"\d+\.\d*", header[1])[0])
                hight = float(re.findall(r"\d+\.\d*", header[2])[0])
                column_names = [clm(i) for i in range(len(data[0]))]
                data = pd.DataFrame(data, columns=column_names)
                data = data.transpose()

                if re.search("_be", _file):
                    _be_ctr += 1
                    before_etch[key + "_" + str(_be_ctr)] = data
                elif re.search("_ae", _file):
                    _ae_ctr += 1
                    after_etch[key + "_" + str(_ae_ctr)] = data
                elif re.search("_al", _file):
                    _al_ctr += 1
                    after_lift[key + "_" + str(_al_ctr)] = data
                else:
                    logger.error(f"Not able to sort File {_file} in one of the expected Categories. Categories must be specified within filename. See documentation")

    logger.info("Reading data finished. Calculation of propability density function starts\n\n")
    tmp_dict = calcStats(before_etch)
    before_etch.update(tmp_dict)
    tmp_dict = calcStats(after_etch)
    after_etch.update(tmp_dict)
    tmp_dict = calcStats(after_lift)
    after_lift.update(tmp_dict)

    for key in sample_keys:
        be_key_gen = (x for x in before_etch.keys() if key in x and "_stats" in x)
        ae_key_gen = (x for x in after_etch.keys() if key in x  and "_stats" in x)
        al_key_gen = (x for x in after_lift.keys() if key in x and "_stats" in x)

        logger.info(f"{key} before etching stats:\n")
        tmp_dict = getMeanStats(be_key_gen, key, before_etch, key)
        if not bool(tmp_dict):
            logger.warning(f"No measurement for before etching was found for {key}.")
        else: before_etch.update(tmp_dict)
        logger.info(f"{key} after etching stats:\n")
        tmp_dict = getMeanStats(ae_key_gen, key, after_etch, key)
        if not bool(tmp_dict):
            logger.warning(f"No measurement for after etching was found for {key}.")
        else: after_etch.update(tmp_dict)
        logger.info(f"{key} after lift-off stats:\n")
        tmp_dict = getMeanStats(al_key_gen, key, after_lift, key)
        if not bool(tmp_dict):
            logger.warning(f"No measurement for after lift-off was found for {key}.")
        else: after_lift.update(tmp_dict)

        # Calculating etching rates
        time = after_etch[key + "_1_stats"]["etching_time"]
        etch_ratios = calculateEtchRatios(before_etch, after_etch, after_lift, time, key)
        etch_ratio_dict[key] = {
            "etching rate diamant": round((etch_ratios['modus_diamant']*1e9), 1),
            "etching rate resin": round((etch_ratios['modus_resin']*1e9), 1),
            "coefficient alpha": round(etch_ratios['modus_alpha'], 2),
            "coefficient beta": round(etch_ratios['modus_beta'], 2)
            }

        if int(CALCULATION_ADDITIONAL_OUTPUTS, base=2) == 1:
            median_etch_ratio_dict[key] = {
                "etching rate diamant": round((etch_ratios['median_diamant']*1e9), 1),
                "etching rate resin": round((etch_ratios['median_resin']*1e9), 1),
                "coefficient alpha": round(etch_ratios['median_alpha'], 2),
                "coefficient beta": round(etch_ratios['median_beta'], 2)
                }
        elif int(CALCULATION_ADDITIONAL_OUTPUTS, base=2) == 2:
            mean_etch_ratio_dict[key] = {
                "etching rate diamant": round((etch_ratios['mean_diamant']*1e9), 1),
                "etching rate resin": round((etch_ratios['mean_resin']*1e9), 1),
                "coefficient alpha": round(etch_ratios['mean_alpha'], 2),
                "coefficient beta": round(etch_ratios['mean_beta'], 2)
                }
        elif int(CALCULATION_ADDITIONAL_OUTPUTS, base=2) == 3:
            mean_etch_ratio_dict[key] = {
                "etching rate diamant": round((etch_ratios['mean_diamant']*1e9), 1),
                "etching rate resin": round((etch_ratios['mean_resin']*1e9), 1),
                "coefficient alpha": round(etch_ratios['mean_alpha'], 2),
                "coefficient beta": round(etch_ratios['mean_beta'], 2)
                }
            median_etch_ratio_dict[key] = {
                "etching rate diamant": round((etch_ratios['median_diamant']*1e9), 1),
                "etching rate resin": round((etch_ratios['median_resin']*1e9), 1),
                "coefficient alpha": round(etch_ratios['median_alpha'], 2),
                "coefficient beta": round(etch_ratios['median_beta'], 2)
            }

    for key in sample_keys:
        logger.info(f"The calculated etching rates for sample {key} are:\nDiamant:\t{etch_ratio_dict[key]['etching rate diamant']} nm/min\nResin:\t\t{etch_ratio_dict[key]['etching rate resin']} nm/min\n\nEtching rate coefficients:\nAlpha:\t\t{etch_ratio_dict[key]['coefficient alpha']}\nBeta: \t\t{etch_ratio_dict[key]['coefficient beta']}\n")

    if ENABLE_CSV_OUTPUT:
        modebase = int(CALCULATION_ADDITIONAL_OUTPUTS, base=2)
        if modebase == 0:
            error = WriteDictToCSV(etch_ratio_dict)
        elif modebase == 1:
            error = WriteDictToCSV(etch_ratio_dict, median=True,
            medianbase=median_etch_ratio_dict)
        elif modebase == 2:
            error = WriteDictToCSV(etch_ratio_dict, median=True,
            meanbase=mean_etch_ratio_dict)
        elif modebase == 3:
            error = WriteDictToCSV(etch_ratio_dict, median=True, mean=True,
            medianbase=median_etch_ratio_dict, meanbase=mean_etch_ratio_dict)

        if error != "0":
            logger.error(error)
            error_counter += 1

        if error_counter > 0:
            logger.warning(f"\nProgram finished with {error_counter} errors.")
        else:
            logger.info("\nProgram finished without any errors")
# **____________________** #


# ** Script ** #
if __name__ == "__main__":
    main()
# **____________________** #