Program: AFM Custom Heightdest recognition v1.0

This program can calculate the etching rates of to partially stacked materials after an etching step,
even if the surface of the materials is rough. For doing this the data from three AFM measurements is needed:
One measurement each before the etching process, after the etching process and after removing the upper material.
The program is designed to calculate the etching rates for resin based structures on diamond.
But it should also work for any other substrate material.
The AFM measurements must include an edge of the upper material on it to recognize both height levels.
The image should be rotated in order that the edge runs from the top to the bottom of the image,
so that in each line contains data about both height levels. It works best if a cut out of the AFM data around the edge is given.

Getting started:

1. Make sure all needed python libraries are installed:

    You can install them with the help of the "requirements.txt" file in the program folder.
    Therefore it is recommended to use a virtual environment.
    Otherwise it is possible that your system environment is changed in a way that other python applications wont work any longer.

    With your virtual environment activated in your terminal:
    type "pip install -r <path>/requirements.txt"
    This will add all required packages in their needed versions to your environment.

2. Setting the correct interpreter to run the program:

    With your environment prepared make sure to use the environment interpreter to run the program. If needed change the interpreter.
    It is enough to activate the correct environment in your terminal.
    In the IDE Visual Studio Code you can select the environment as following:
        pressing:   "F1"
        typing:     "Python: Select Interpreter"
        pressing:   "Return"
        selecting:  interpreter from the dropdown list
        pressing:   "Return"

    If the interpreter is not listed in the dropdown list you can select
    "Find:" and search for the corresponding python.exe in the explorer.

3. Setting up all parameters:

    In the current version you need to set your settings within the source code. To do this open the analyze_resin_edge.py.
    In the section parameters all the changeable options are defined.
    There are docstrings for each parameter with a short explanation of its function and its possible values.
    Set up the the parameters as needed for your requirements.

4. Preparing your measurement data for analysis:

    The program expects the data in a specific structure. Any other structure may lead to unexpected behavior and wrong results.

    1.  The measurement data you wish to analyze must be placed in a subfolder called "daten" under the analyze_resin_edge.py
    2.  In the "daten" folder create subfolders for each sample you wish to analyze.
        The subfolder names should match your sample names to remain clarity
    3.  Your sample data must be in CSV format.
    4.  The file name must have the following structure:

        <sample name>_<measurement count>_<measurement>{_<additional information>}.csv

        The "<sample name>" section holds the name of your sample

        The "<measurement count>" is the number of the measurement of the same sample.
        The program will calculate the mean of all calculated heights of the same samples with different measurement counts.

        The options for "<measurement>" are:
            "be" to mark measurement as before etching process
            "ae" to mark measurement as after etching process
            "al" to mark measurement as after resin lift-off

        In the "<additional information>" section you can code any information you like to have in you file name.
        Currently it wont have any effect on the program. The part in {} can be skipped.

    NOTE:       To generate all expected information about your sample at least one file for all
                measurement modes is required.

    Example:    The Filename: "A1_1_ae_40o280ar.csv" Tells the program that this file holds the
                information about the first measured edge after the etching process of the sample A1.

5. Run the program

    If output csv is activated the program creates a folder "results" with a subfolder named as the current date.
    The current date folder contains the CSV file with the results. The file name can be defined in the analyze_resin_edge.py

    NOTE:       To protect your work from accidental overwriting,
                executing the program more than onetime on the same day with the same result file name will cause an error.
                The data of the first execution is kept and the data of any next execution wont be saved.
                To save data of more than one execution per day simply define a new result filename in the analyze_resin_edge.py.
                You can also rename the existing result file or drag it into an other folder.

    If logging in file is activated, a folder "log" is created with a log file
    The log file contains additional information about the program sequence depending on your settings for DEBUG_LEVEL

    NOTE:       The DEBUG_LEVEL should be configured that at least warnings are shown,
                so the program informs you about unexpected results while the analysis of your samples is running.
                It can also be useful to log at INFO level.
                This let's you keeping track of the current execution step and gives some important interim results.

6. Troubleshooting:

    If something goes wrong check the log information. Try to execute the program again with the DEBUG_LEVEL at 10 for debug_mode.
    This adds entries about the results of almost every calculation step.
    You can also activate some debug plots to check the proper function of the program at different states.
    This may lead to very long execution time.