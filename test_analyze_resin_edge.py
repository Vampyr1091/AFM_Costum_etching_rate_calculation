import unittest
import os
import numpy as np


os.chdir("AFM Custom Heightdest recognition")
import analyze_resin_edge


class Test_analyze_resin_edge(unittest.TestCase):

    def test_peakFinder(self):
        test_array = np.array([(-x**2 + 100) for x in range(-10,11,1)])
        self.assertEqual(analyze_resin_edge.peakFinder(test_array), [10])




if __name__ == "__main__":
    unittest.main()